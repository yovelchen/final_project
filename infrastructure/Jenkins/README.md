# Jenkins

 setup Jenkins on Kubernetes Cluster with Dynamic Agents / Pods

## Prerequisites
1. Kubernetes cluster
2. Helm client 
3. **make sure your Kubenetes version is 1.24 and up**

## Steps
1. helm repo add jenkins https://charts.jenkins.io
2. helm repo update
3. create [serviceAccount.yaml](https://gitlab.com/sela-1090/students/yovelchen/infrastructures/jenkins/-/blob/Use-Kubernetes-Pods-As-Jenkins-Agents/Use%20Kubernetes%20Pods%20As%20Jenkins%20Agents/serviceAccount.yaml),
[jenkins-values.yaml](https://gitlab.com/sela-1090/students/yovelchen/infrastructures/jenkins/-/blob/Use-Kubernetes-Pods-As-Jenkins-Agents/Use%20Kubernetes%20Pods%20As%20Jenkins%20Agents/jenkins-values.yaml)
* make sure you add all the desired plugins to your jenkins-values.yaml 
```
installPlugins:
    - kubernetes:3937.vd7b_82db_e347b_
    - workflow-multibranch:756.v891d88f2cd46
    - docker-pipeline
```

 
4. helm install jenkins -n cicd -f jenkins-values.yaml jenkins/jenkins
5. create a pipeline (you can add this code if you dont heve one ready [pipelineExemple](pipelineExemple))
6. go to powershell and run > kubectl get pods -n cicd --watch 
* you will see the dynamic agent pods going up while the pipeline is running, and destroyed when the pipeline job is done. 
___________________________________________________________________________________
### Create a service account and token for jenkins in the cluster: 
1. kubectl apply -f serviceAccount.yaml -n cicd 
2. kubectl create token jenkins -n cicd
* the token is available for one hour unless specify otherwise 
for 11.5 days: kubectl create token jenkins -n cicd --duration=1000000s
3. in Jenkins go to manage jenkins > nodes and cloud > clouds > Kubernetes > kubernetes cloud details (edit) > keep default > Disable https certificate check
4. add credentials > kind: secret text > secret: apply token from 6 > id +description you choose 
5. choose Credentials
6. TEST > if message is: Connected to Kubernetes v1.27.1 > your good 
---------------------------------------------------------------------

## Credits
1. https://www.jenkins.io/doc/book/installing/kubernetes/
2. https://www.youtube.com/watch?v=mzm7prM4f64&t=335s&ab_channel=DevopsGuru
3. https://www.youtube.com/watch?v=ZXaorni-icg&t=639s&ab_channel=CloudBeesTV
4. https://www.youtube.com/watch?v=vk0EIznJJe0&t=163s&ab_channel=Kubesimplify
