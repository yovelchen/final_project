# Observation

## Getting started

1. helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
2. helm repo add grafana https://grafana.github.io/helm-charts
3. helm install prometheus prometheus-community/prometheus -n observation  
* make sure you expose it to a diffrent port if you run jenkins as well (app port:your port)

get Grafana password :  kubectl get secret --namespace observation my-release-grafana -o jsonpath="{.data.admin-password}" | base64 --decode 
