# Final Project 

![project](extra-information/image.png)

## Infrastructure 

- INF-01 - Kubernetes cluster establishment [link](https://gitlab.com/yovelchen/final_project/-/tree/main/infrastructure/Kubernetes)
- INF-02 - Jenkins server establishment
- INF-04 - ArgoCD solution establishment
- INF-05 - Prometheus and Grafana solutions establishment
- INF-06 - Create all needed namespaces and deploy an ingress
- INF-07 - PostgreSQL database establishment

## Application 

- APP-01 - The application written in Python3
- APP-02 - Docker-application
- APP-03 - features unit-tests
- APP-04 - Database usage
- APP-05 - Web-frontend
- APP-06 - HELM-deployment

## Pipeline 

- PIPE-01 - feature-branch Jenkins CI-pipeline
- PIPE-02 - main-branch Jenkins CI-pipeline
- PIPE-03 - CD pipeline for each environment

## Bonuses

- BON-01 - Integration of  generative-AI tool through an API. [link](https://gitlab.com/yovelchen/final_project/-/blob/main/AI-API.md)
- BON-02 - Implement a cluster-portal, which is a web-entry with links to all the CI/CD and monitoring services. [link](https://github.com/yovelchen/Kubernetes_portal)
